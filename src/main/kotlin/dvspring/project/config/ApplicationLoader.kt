package dvspring.project.config

import dvspring.project.entity.*
import dvspring.project.repository.TransactionRepository
import dvspring.project.repository.UserRepository
import dvspring.project.repository.WalletRepository
import dvspring.project.security.entity.Authority
import dvspring.project.security.entity.AuthorityName
import dvspring.project.security.entity.JwtUser
import dvspring.project.security.repository.AuthorityRepository
import dvspring.project.security.repository.JwtUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import java.time.LocalDate
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner {
    @Autowired
    lateinit var walletRepository: WalletRepository
    @Autowired
    lateinit var transactionRepository: TransactionRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var jwtUserRepository: JwtUserRepository


    @Transactional
    override fun run(arg: ApplicationArguments?) {
                var wallet1 = walletRepository.save(Wallet("Day3",500.0))
        var wallet2 = walletRepository.save(Wallet("Day4",800.0))
        var wallet3 = walletRepository.save(Wallet("Extra money",5000.0))

        var transaction1 = transactionRepository.save(Transaction("From mommy", 500.0,
                TransactionStatus.INCOME,TransactionCategoryStatus.INCOME, LocalDate.of(2019,3,22)))
        var transaction2 = transactionRepository.save(Transaction("From Daddy", 600.0,
                TransactionStatus.INCOME,TransactionCategoryStatus.INCOME, LocalDate.of(2019,3,21)))
        var transaction3 = transactionRepository.save(Transaction("Buy snack", 100.0,
                TransactionStatus.EXPENSE,TransactionCategoryStatus.FOOD, LocalDate.of(2019,3,21)))
        var transaction4 = transactionRepository.save(Transaction("Play games", 200.0,
                TransactionStatus.EXPENSE,TransactionCategoryStatus.TRAVEL, LocalDate.of(2019,3,21)))

        wallet1.transactions.add(transaction1)
        wallet1.transactions.add(transaction3)
        wallet2.transactions.add(transaction2)
        wallet2.transactions.add(transaction4)

        var user1 = userRepository.save(User("jaja","isHappy","jaja@gmail.com",null))

        user1.wallets.add(wallet1)
        user1.wallets.add(wallet2)


        @Transactional
        fun loadUsernameAndPassword() {
            val auth1 = Authority(name = AuthorityName.ROLE_CUSTOMER)
            val auth2 = Authority(name = AuthorityName.ROLE_GENERAL)
            authorityRepository.save(auth1)
            authorityRepository.save(auth2)
            val encoder = BCryptPasswordEncoder()
            val user1 = User( firstName = "jaja", lastName = "isHappy")
            val userJwt = JwtUser(
                    username = "jaja@gmail.com",
                    password = encoder.encode("jaja"),
                    email = user1.username,
                    enabled = true,
                    firstname = user1.firstName,
                    lastname = user1.lastName
            )
            userRepository.save(user1)
            jwtUserRepository.save(userJwt)
            user1.jwtUser = userJwt
            userJwt.user = user1
            userJwt.authorities.add(auth1)

        }
        loadUsernameAndPassword()

    }


}