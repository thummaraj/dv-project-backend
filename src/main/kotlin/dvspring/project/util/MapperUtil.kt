package dvspring.project.util

import dvspring.project.entity.Register
import dvspring.project.entity.Transaction
import dvspring.project.entity.User
import dvspring.project.entity.Wallet
import dvspring.project.entity.dto.*
import dvspring.project.security.entity.Authority
import io.jsonwebtoken.Jwt
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
    @Mappings(
            Mapping(source = "wallets", target = "walletList")
    )

    fun mapUserDto(user: User): UserDto
    fun mapUserDto(users: List<User>): List<UserDto>
    @InheritInverseConfiguration
    fun mapUserDto(user: UserDto): User

    @Mappings(
            Mapping(source = "transactions", target = "transactionList")

    )

    fun mapWalletDto(wallet: Wallet): WalletDto
    fun mapWalletDto(wallets: List<Wallet>): List<WalletDto>
    @InheritInverseConfiguration
    fun mapWallet(wallet: WalletDto): Wallet

    fun mapTransactionDto(transaction: Transaction): TransactionDto
    fun mapTransactionDto(transactions: List<Transaction>): List<TransactionDto>
    @InheritInverseConfiguration
    fun mapTransactionDto(transactionDto: TransactionDto): Transaction


    @Mappings(
            Mapping(source = "user.jwtUser.username", target = "username"),
            Mapping(source = "user.jwtUser.authorities", target = "authorities")
    )
    fun mapUser(user: User): JwtUserDto
    @InheritInverseConfiguration
    fun mapRegisUser(regisUserDto: JwtUserDto): User

    fun mapAuthority(authority: Authority): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>







}