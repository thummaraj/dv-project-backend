package dvspring.project.dao

import dvspring.project.entity.Transaction
import dvspring.project.entity.TransactionStatus
import dvspring.project.repository.TransactionRepository
import dvspring.project.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.lang.IllegalArgumentException
import java.time.LocalDate

@Repository
class TransactionDaoDBlmpl : TransactionDao{
    override fun getTransactionByDate(date: String): List<Transaction> {
        val localDate = LocalDate.parse(date)
        return transactionRepository.findByDate(localDate.toString())
    }

    override fun getTransactionByStatus(status: String): List<Transaction> {
        try {
            return transactionRepository.findByTransactionStatus(TransactionStatus.valueOf(status.toUpperCase()))
        }catch (e: IllegalArgumentException){
            return emptyList()
        }
    }

    override fun getTransaction(): List<Transaction> {
        return transactionRepository.findByIsDeletedIsFalse()
    }

    override fun findById(id: Long): Transaction {
        val transaction = transactionRepository.findById(id)
        return transaction.get()
    }

    override fun save(transDto: Transaction): Transaction {
        return transactionRepository.save(transDto)
    }

    @Autowired
    lateinit var transactionRepository: TransactionRepository

}