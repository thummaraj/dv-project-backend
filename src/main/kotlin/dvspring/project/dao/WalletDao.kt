package dvspring.project.dao

import dvspring.project.entity.Wallet

interface WalletDao {
    fun getWallets(): List<Wallet>
    fun getWalletByPartialName(name: String): List<Wallet>
    fun getWalletByStatus(status: String): List<Wallet>
    fun getWalletByCategory(category: String): List<Wallet>
    fun save(wallet: Wallet): Wallet
    fun findById(walletID: Long): Wallet

}