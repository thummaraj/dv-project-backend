package dvspring.project.dao

import dvspring.project.entity.Transaction
import dvspring.project.entity.TransactionCategoryStatus
import dvspring.project.entity.TransactionStatus
import dvspring.project.entity.Wallet
import dvspring.project.entity.dto.TransactionDto
import dvspring.project.repository.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.lang.IllegalArgumentException

@Repository
class WalletDaoDBlmpl : WalletDao{
    override fun findById(walletID: Long): Wallet {
        return walletRepository.findById(walletID).orElse(null)
    }

    override fun save(wallet: Wallet): Wallet {
        return walletRepository.save(wallet)
    }

    override fun getWalletByCategory(category: String): List<Wallet> {
        try {
            return walletRepository.findByTransactions_TransactionCategoryStatus(TransactionCategoryStatus.valueOf(category.toUpperCase()))
        }catch (e:IllegalArgumentException){
            return emptyList()
        }
    }

    override fun getWalletByStatus(status: String): List<Wallet> {
        try {
            return walletRepository.findByTransactions_TransactionStatus(TransactionStatus.valueOf(status.toUpperCase()))
        }catch (e:IllegalArgumentException){
            return emptyList()
        }
    }

    override fun getWalletByPartialName(name: String): List<Wallet> {
        return walletRepository.findByWalletNameContainingIgnoreCase(name)
    }
    @Autowired
    lateinit var walletRepository: WalletRepository
    override fun getWallets(): List<Wallet> {
//            return walletRepository.findAll().filterIsInstance(Wallet::class.java)
        return walletRepository.findByIsDeletedIsFalse()


    }

}