package dvspring.project.dao

import dvspring.project.entity.Register
import dvspring.project.entity.User
import dvspring.project.entity.dto.JwtUserDto
import dvspring.project.entity.dto.RegisterDto
import dvspring.project.entity.dto.UserDto

interface UserDao {
     fun getUser(): List<User>
     fun save(user: User): User
     fun findById(id: Long): User
     fun saveRegister(regisUser: User): User
}