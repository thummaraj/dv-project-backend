package dvspring.project.dao

import dvspring.project.entity.Register
import dvspring.project.entity.User
import dvspring.project.entity.dto.JwtUserDto
import dvspring.project.entity.dto.RegisterDto
import dvspring.project.entity.dto.UserDto
import dvspring.project.repository.UserRepository
import dvspring.project.security.entity.Authority
import dvspring.project.security.entity.AuthorityName
import dvspring.project.security.entity.JwtUser
import dvspring.project.security.repository.AuthorityRepository
import dvspring.project.security.repository.JwtUserRepository
import dvspring.project.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional


@Repository
class UserDaoDBlmpl: UserDao {
    @Autowired
    lateinit var jwtUserRepository: JwtUserRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Transactional
    override fun saveRegister(regisUser: User): User {
        val roleUser = Authority(name = AuthorityName.ROLE_CUSTOMER)
        authorityRepository.save(roleUser)
        val encoder = BCryptPasswordEncoder()
        val userl = regisUser
        val userJwt = JwtUser(
                username = userl.username,
                password = encoder.encode(userl.password),
                email = userl.username,
                enabled = true,
                firstname = userl.firstName,
                lastname = userl.lastName
        )
        userRepository.save(userl)
        jwtUserRepository.save(userJwt)
        userl.jwtUser = userJwt
        userJwt.user = userl
        userJwt.authorities.add(roleUser)

        return userRepository.save(regisUser)

    }

    override fun findById(id: Long): User {
        val user = userRepository.findById(id)
        return user.get()
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }
    @Autowired
    lateinit var userRepository: UserRepository
    override fun getUser(): List<User> {
//        return userRepository.findAll().filterIsInstance(User::class.java)
        return userRepository.findByIsDeletedIsFalse()
    }
}