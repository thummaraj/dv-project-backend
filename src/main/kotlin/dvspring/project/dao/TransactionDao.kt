package dvspring.project.dao

import dvspring.project.entity.Transaction
import java.time.LocalDate

interface TransactionDao {
     fun save(transDto: Transaction): Transaction
     fun findById(id: Long): Transaction
     fun getTransaction(): List<Transaction>
     fun getTransactionByStatus(status: String): List<Transaction>
     fun getTransactionByDate(date: String): List<Transaction>

}