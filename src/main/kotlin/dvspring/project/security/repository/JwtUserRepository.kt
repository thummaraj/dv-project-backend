package dvspring.project.security.repository

import dvspring.project.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface JwtUserRepository : CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}