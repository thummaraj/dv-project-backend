package dvspring.project.security.repository

import dvspring.project.security.entity.Authority
import dvspring.project.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}