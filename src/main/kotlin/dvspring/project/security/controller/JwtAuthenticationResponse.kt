package dvspring.project.security.controller

data class JwtAuthenticationResponse(
        var token: String? = null
)