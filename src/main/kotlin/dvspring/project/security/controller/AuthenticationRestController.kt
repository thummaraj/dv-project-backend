package dvspring.project.security.controller

import dvspring.project.entity.User
import dvspring.project.security.JwtTokenUtil
import dvspring.project.security.entity.JwtUser
import dvspring.project.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.mobile.device.Device
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.transaction.Transactional

@RestController
class AuthenticationRestController{
    @Value("\${jwt.header}")
    private val tokenHeader: String? = null

    @Autowired
    lateinit var authenticationManager: AuthenticationManager

    @Autowired
    lateinit var jwtTokenUtil: JwtTokenUtil

    @Autowired
    lateinit var userDetailsService: UserDetailsService


    @PostMapping("\${jwt.route.authentication.path}")
    @Throws(AuthenticationException::class)
    @Transactional
    fun createAuthenticationToken(@RequestBody authenticationRequest: JwtAuthenticationRequest, device: Device): ResponseEntity<*> {

        // Perform the security
        val authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                        authenticationRequest.username,
                        authenticationRequest.password)
        )

        SecurityContextHolder.getContext().authentication = authentication

        // Reload password post-security so we can generate token
        val userDetails = userDetailsService.loadUserByUsername(authenticationRequest.username)
        val token = jwtTokenUtil.generateToken(userDetails, device)
        val result = HashMap<String,Any>()
        result.put("token", token)
        val currentUser = (userDetails as JwtUser).user
        when(currentUser){
            is User -> currentUser?.let { result.put("user",MapperUtil.INSTANCE.mapUserDto(currentUser)) }
        }

        return ResponseEntity.ok(result)
    }


    @GetMapping("\${jwt.route.authentication.refresh}")
    fun refreshAndGetAuthenticationToken(request: HttpServletRequest): ResponseEntity<*> {
        val token = request.getHeader(tokenHeader)
        val username = jwtTokenUtil.getUsernameFromToken(token)
        val user = userDetailsService.loadUserByUsername(username) as JwtUser

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.lastPasswordResetDate)) {
            val refreshedToken = jwtTokenUtil.refreshToken(token)
            return ResponseEntity.ok(JwtAuthenticationResponse(refreshedToken))
        } else {
            return ResponseEntity.badRequest().body<Any>(null)
        }
    }
}