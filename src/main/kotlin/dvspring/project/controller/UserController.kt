package dvspring.project.controller

import dvspring.project.entity.Register
import dvspring.project.entity.User
import dvspring.project.entity.dto.JwtUserDto
import dvspring.project.entity.dto.RegisterDto
import dvspring.project.entity.dto.UserDto
import dvspring.project.entity.dto.WalletDto
import dvspring.project.service.AmazonClient
import dvspring.project.service.UserService
import dvspring.project.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
class UserController {
    @Autowired
    lateinit var userService: UserService
    @Autowired
    lateinit var amazonClient: AmazonClient
    //------------------------get All users--------------------------------
    @GetMapping("/users")
    fun getAllUsers(): ResponseEntity<Any> {
        val users = userService.getUser()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(users))
    }
    //------------------------get user by id--------------------------------
    @GetMapping("/user/{userID}")
    fun getUserById(@PathVariable("userID") id:Long): ResponseEntity<Any> {
        val users = userService.getUserById(id)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(users))
    }
    //------------------------Register ?--------------------------------
    @PostMapping("/user/addUser")
    fun addUser(@RequestBody regisUser: User): ResponseEntity<Any>{
        val output = userService.saveRegister(regisUser)
        val outputDto = MapperUtil.INSTANCE.mapUser(output)
        return ResponseEntity.ok(outputDto)
    }
    //------------------------delete user--------------------------------
    @DeleteMapping("/user/{userID}/deleteUser")
    fun deleteUser(@PathVariable("userID") id:Long): ResponseEntity<Any>{
        val user = userService.remove(id)
        val outputUser = MapperUtil.INSTANCE.mapUserDto(user)
        outputUser?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the user id is not found")
    }
    //------------------------edit user profile--------------------------------
    @PutMapping("/user/{userID}/editProfile")
    fun editUser(@PathVariable("userID") id:Long,
                 @RequestBody user: UserDto): ResponseEntity<Any>{
        val output =  MapperUtil.INSTANCE.mapUserDto(userService.editUser(id,user))
        return ResponseEntity.ok(output)
    }
    //------------------------update user imageUrl--------------------------------
    @PutMapping("/user/{userID}/addImageUrl")
    fun updateProfile(@RequestPart("file") file: MultipartFile,
                      @PathVariable("userID") id:Long): ResponseEntity<Any>{
        val imageUrl = this.amazonClient.uploadFile(file)
        val output = MapperUtil.INSTANCE.mapUserDto(userService.setProfilePicture(id,imageUrl))
        return ResponseEntity.ok(output)
    }
}