package dvspring.project.controller


import dvspring.project.entity.Transaction
import dvspring.project.entity.Wallet
import dvspring.project.entity.dto.WalletDto
import dvspring.project.service.WalletService
import dvspring.project.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class WalletController {
    @Autowired
    lateinit var walletService: WalletService
    //------------------------get All wallets--------------------------------
    @GetMapping("/wallets")
    fun getAllWallets(): ResponseEntity<Any> {
        val wallets = walletService.getWallets()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapWalletDto(wallets))
    }
    //------------------------get wallet by id--------------------------------
    @GetMapping("/wallet/{walletID}")
    fun getWalletByID(@PathVariable("walletID") id: Long): ResponseEntity<Any> {
        val wallet = walletService.getWalletByID(id)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapWalletDto(wallet))
    }
    //------------------------add wallets--------------------------------
    @PostMapping("/wallet/{userID}/addWallet")
    fun addTransaction(@RequestBody walletDto: WalletDto,
                       @PathVariable userID: Long
    ): ResponseEntity<Any>{
        val output = walletService.save(userID,MapperUtil.INSTANCE.mapWallet(walletDto))
        val outputDto = MapperUtil.INSTANCE.mapWalletDto(output)
        outputDto?.let { return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    //------------------------edit wallet--------------------------------
    @PutMapping("/wallet/updateWallet/{walletID}")
    fun updateWallet(@PathVariable("walletID") id:Long,
                     @RequestBody wallet: WalletDto): ResponseEntity<Any>{
        val output = walletService.editWallet(id,wallet)
        return ResponseEntity.ok(output)
    }
    //------------------------delete wallet--------------------------------
    @DeleteMapping("/wallet/deleteWallet/{walletID}")
    fun deleteTransaction(@PathVariable("walletID") id:Long): ResponseEntity<Any>{
        val wallet = walletService.remove(id)
        val outputWallet = MapperUtil.INSTANCE.mapWalletDto(wallet)
        outputWallet?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the wallet id is not found")
    }
}