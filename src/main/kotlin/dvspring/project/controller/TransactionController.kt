package dvspring.project.controller

import dvspring.project.entity.dto.TransactionDto
import dvspring.project.service.AmazonClient
import dvspring.project.service.TransactionService
import dvspring.project.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.time.LocalDate

@RestController
class TransactionController {
    @Autowired
    lateinit var transactionService: TransactionService
    @Autowired
    lateinit var amazonClient: AmazonClient
    //------------------------get transactions--------------------------------
    @GetMapping("/transactions")
    fun getTransaction(): ResponseEntity<Any>{
        val transaction = transactionService.getTransaction()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapTransactionDto(transaction))
    }

    //------------------------add transactions--------------------------------
    @PostMapping("/trans/{walletID}/addTrans")
    fun addTransaction(@RequestBody transDto: TransactionDto,
                       @PathVariable walletID: Long
                      ): ResponseEntity<Any>{
        val output = transactionService.save(walletID,MapperUtil.INSTANCE.mapTransactionDto(transDto))
        val outputDto = MapperUtil.INSTANCE.mapTransactionDto(output)
        outputDto?.let { return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    //------------------------edit transactions--------------------------------
    @PutMapping("/trans/editTrans/{transID}")
    fun editTransaction(@PathVariable("transID") id: Long,
                        @RequestBody transDto: TransactionDto): ResponseEntity<Any> {
        transDto.id = id
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapTransactionDto(transactionService.save(transDto)))
    }
    //------------------------delete transactions--------------------------------
    @DeleteMapping("/trans/deleteTrans/{transID}")
    fun deleteTransaction(@PathVariable("transID") id:Long): ResponseEntity<Any>{
        val transaction = transactionService.remove(id)
        val outputTrans = MapperUtil.INSTANCE.mapTransactionDto(transaction)
        outputTrans?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the transaction id is not found")
    }
    //------------------------upload imageUrl in transaction--------------------------------
    @PutMapping("/trans/updateTrans/{transID}")
    fun updateTransactionPicture(@RequestPart("file") file: MultipartFile,
                      @PathVariable("transID") id:Long): ResponseEntity<Any>{
        val imageUrl = this.amazonClient.uploadFile(file)
        return ResponseEntity.ok(transactionService.addTransactionPicture(id,imageUrl))
    }
    //------------------------get transaction by status(income,expense)--------------------------------
    @GetMapping("/trans/{status}")
    fun getTransactionByStatus(@PathVariable("status") status:String): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapTransactionDto(transactionService.getTransactionByStatus(status))
        return ResponseEntity.ok(output)
    }
}