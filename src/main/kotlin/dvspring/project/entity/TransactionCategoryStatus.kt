package dvspring.project.entity

enum class TransactionCategoryStatus {
    FOOD,DRUG,CLOTHES,TRAVEL,INCOME
}