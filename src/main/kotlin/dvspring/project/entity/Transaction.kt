package dvspring.project.entity

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Transaction (var title: String?= null,
                        var amountOfMoney: Double? = null,
                        var transactionStatus: TransactionStatus? = TransactionStatus.INCOME,
                        var transactionCategoryStatus: TransactionCategoryStatus? = TransactionCategoryStatus.FOOD,
                        var date: LocalDate? = null,
                        var imageUrl: String?= null,
                        var isDeleted: Boolean = false
                        ){

    @Id
    @GeneratedValue
    var id:Long? = null
}