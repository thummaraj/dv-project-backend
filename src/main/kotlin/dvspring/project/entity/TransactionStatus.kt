package dvspring.project.entity

enum class TransactionStatus {
    INCOME, EXPENSE
}