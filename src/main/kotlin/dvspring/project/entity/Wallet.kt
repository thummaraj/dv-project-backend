package dvspring.project.entity

import javax.persistence.*

@Entity
data class Wallet (var walletName: String?= null,
                   var budget: Double?= null,
                   var isDeleted: Boolean = false
                   ){
    @Id
    @GeneratedValue
    var id:Long?= null
    @OneToMany
    var transactions = mutableListOf<Transaction>()
}