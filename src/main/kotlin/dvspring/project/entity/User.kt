package dvspring.project.entity

import dvspring.project.security.entity.JwtUser
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
class User(
         open var firstName: String?= null,
         open var lastName: String?= null,
         open var username:String?= null,
         open var imageUrl: String?= null,
         open var isDeleted: Boolean = false,
         var password:String?= null

                 ){
    @Id
    @GeneratedValue
    var id:Long? = null

    @OneToMany
    var wallets = mutableListOf<Wallet>()

    @OneToOne
    var jwtUser: JwtUser?= null
}
