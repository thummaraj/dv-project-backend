package dvspring.project.entity

data class Register (var firstName:String,
                     var lastName:String,
                     var username:String,
                     var password:String,
                     var confirmpassword:String)