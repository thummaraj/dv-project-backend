package dvspring.project.entity.dto

data class UserDto (var firstName: String?= null,
                    var lastName: String?= null,
                    var username:String?= null,
                    var imageUrl: String?= null,
                    var walletList:  List<WalletDto>?= emptyList(),
                    var id:Long? = null)