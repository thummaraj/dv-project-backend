package dvspring.project.entity.dto

data class WalletDto (var walletName: String?= null,
                      var budget: Double?= null,
                      var transactionList: List<TransactionDto>?= emptyList(),
                      var id:Long?= null
                     )