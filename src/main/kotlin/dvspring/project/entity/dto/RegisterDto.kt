package dvspring.project.entity.dto

data class RegisterDto (var firstName:String?= null,
                        var lastName:String?= null,
                        var username:String?= null,
                        var authorities: List<AuthorityDto>? = mutableListOf()
)