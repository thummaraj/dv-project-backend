package dvspring.project.entity.dto

data class JwtUserDto (var firstName:String?= null,
                       var lastName:String?= null,
                       var username: String?=null,
                       var authorities: List<AuthorityDto> = mutableListOf(),
                       var id:Long?= null)