package dvspring.project.entity.dto

import dvspring.project.entity.TransactionCategoryStatus
import dvspring.project.entity.TransactionStatus
import java.time.LocalDate

data class TransactionDto (var title: String? = null,
                           var amountOfMoney: Double?= null,
                           var transactionStatus: TransactionStatus? = TransactionStatus.INCOME,
                           var transactionCategoryStatus: TransactionCategoryStatus? = TransactionCategoryStatus.FOOD,
                           var date: LocalDate? = LocalDate.now(),
                           var imageUrl: String?= null,
                           var id:Long?= null
                           )