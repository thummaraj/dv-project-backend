package dvspring.project.service

import dvspring.project.dao.TransactionDao
import dvspring.project.dao.UserDao
import dvspring.project.dao.WalletDao
import dvspring.project.entity.Transaction
import dvspring.project.entity.Wallet
import dvspring.project.entity.dto.WalletDto
import dvspring.project.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class WalletServicelmpl: WalletService {
    override fun editWallet(id: Long, wallet: WalletDto): Wallet {
        val walletEdit = walletDao.findById(id)
        if(walletEdit.walletName != null){
            walletEdit.walletName = wallet.walletName
        } else {
            wallet.walletName = wallet.walletName
        }
        return walletDao.save(walletEdit)

    }

    @Autowired
    lateinit var userDao: UserDao
    @Transactional
    override fun save(userID: Long, walletDto: Wallet): Wallet {
        val user = userDao.findById(userID)
        val wallet = walletDao.save(walletDto)
        user?.wallets.add(wallet)
        if(wallet.walletName == null){
            wallet.walletName = "New Wallet"
        }
        if (wallet.budget != null){
        } else{
            wallet.budget = 0.0
        }
        return wallet
    }

    override fun getWalletByID(id: Long): Wallet {
        return walletDao.findById(id)
    }

    @Transactional
    override fun remove(id: Long): Wallet {
        val wallet = walletDao.findById(id)
        wallet?.isDeleted = true
        return wallet
    }

    override fun save(wallet: WalletDto): Wallet {
        val wallet =  MapperUtil.INSTANCE.mapWallet(wallet)
        return walletDao.save(wallet)
    }

    override fun getWalletByCategory(category: String): List<Wallet> {
        return walletDao.getWalletByCategory(category)
    }

    override fun getWalletByStatus(status: String): List<Wallet> {
        return walletDao.getWalletByStatus(status)
    }

    override fun getWalletByPartialName(name: String): List<Wallet> {
        return walletDao.getWalletByPartialName(name)
    }
    @Autowired
    lateinit var walletDao: WalletDao
    override fun getWallets(): List<Wallet> {
        return walletDao.getWallets()
    }
}