package dvspring.project.service

import dvspring.project.entity.Transaction
import dvspring.project.entity.dto.TransactionDto
import java.time.LocalDate

interface TransactionService {
     fun save(transDto: TransactionDto): Transaction
     fun save(wallerID: Long, transDto: Transaction): Transaction
     fun remove(id: Long): Transaction
     fun getTransaction(): List<Transaction>
     fun addTransactionPicture(id: Long, imageUrl: String): Transaction
     fun getTransactionByStatus(status: String): List<Transaction>
     fun getTransactionByDate(date: String): List<Transaction>


}