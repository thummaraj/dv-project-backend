package dvspring.project.service

import dvspring.project.entity.Wallet
import dvspring.project.entity.dto.WalletDto

interface WalletService {
    fun getWallets(): List<Wallet>
    fun getWalletByPartialName(name: String): List<Wallet>
    fun getWalletByStatus(status: String): List<Wallet>
    fun getWalletByCategory(category: String): List<Wallet>
    fun save(wallet: WalletDto): Wallet
    fun save(userID: Long, walletDto: Wallet): Wallet
    fun remove(id: Long): Wallet
    fun getWalletByID(id: Long): Wallet
    fun editWallet(id: Long, wallet: WalletDto): Wallet
}