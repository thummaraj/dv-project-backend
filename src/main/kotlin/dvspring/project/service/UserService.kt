package dvspring.project.service

import dvspring.project.entity.Register
import dvspring.project.entity.User
import dvspring.project.entity.Wallet
import dvspring.project.entity.dto.JwtUserDto
import dvspring.project.entity.dto.RegisterDto
import dvspring.project.entity.dto.UserDto
import dvspring.project.entity.dto.WalletDto


interface UserService {
     fun getUser(): List<User>
     fun setProfilePicture(id: Long, imageUrl: String): User
     fun save(user: UserDto): User
     fun remove(id: Long): User
     fun getUserById(id: Long): User
     fun editUser(id: Long, user: UserDto): User
     fun saveRegister(regisUser: User): User
}