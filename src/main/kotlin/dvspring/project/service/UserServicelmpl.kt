package dvspring.project.service

import dvspring.project.dao.UserDao
import dvspring.project.dao.WalletDao
import dvspring.project.entity.Register
import dvspring.project.entity.User
import dvspring.project.entity.Wallet
import dvspring.project.entity.dto.JwtUserDto
import dvspring.project.entity.dto.RegisterDto
import dvspring.project.entity.dto.UserDto
import dvspring.project.entity.dto.WalletDto
import dvspring.project.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class UserServicelmpl: UserService {
    override fun saveRegister(regisUser: User): User {
        return userDao.saveRegister(regisUser)
    }

    override fun editUser(id: Long, user: UserDto): User {
        val userEdit = userDao.findById(id)
        userEdit.firstName = user.firstName
        if(userEdit.firstName != null){
            userEdit.firstName = user.firstName
        } else{
            user.firstName = user.firstName
            if(userEdit.lastName != null){
                userEdit.lastName = user.lastName
            }
            user.lastName = user.lastName
        }
        return userDao.save(userEdit)
    }

    override fun getUserById(id: Long): User {
        return userDao.findById(id)
    }

    override fun remove(id: Long): User {
        val user = userDao.findById(id)
        user?.isDeleted = true
        return user
    }

    override fun save(user: UserDto): User {
        val user = MapperUtil.INSTANCE.mapUserDto(user)
        return userDao.save(user)
    }

    @Transactional
    override fun setProfilePicture(id: Long, imageUrl: String): User {
        val user = userDao.findById(id)
        user.imageUrl = imageUrl
        return userDao.save(user)
    }

    @Autowired
    lateinit var userDao: UserDao

    override fun getUser(): List<User> {
        return userDao.getUser()
    }
}