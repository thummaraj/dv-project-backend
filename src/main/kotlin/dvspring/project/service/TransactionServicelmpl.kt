package dvspring.project.service

import dvspring.project.dao.TransactionDao
import dvspring.project.dao.WalletDao
import dvspring.project.entity.Transaction
import dvspring.project.entity.TransactionStatus
import dvspring.project.entity.dto.TransactionDto
import dvspring.project.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate
import javax.transaction.Transactional

@Service
class TransactionServicelmpl : TransactionService {

    override fun getTransactionByDate(date: String): List<Transaction> {
        return transactionDao.getTransactionByDate(date)
    }

    override fun getTransactionByStatus(status: String): List<Transaction> {
        return transactionDao.getTransactionByStatus(status)
    }

    override fun addTransactionPicture(id: Long, imageUrl: String): Transaction {
        val transaction = transactionDao.findById(id)
        transaction.imageUrl = imageUrl
        return transactionDao.save(transaction)
    }

    @Autowired
    lateinit var transactionDao: TransactionDao

    override fun getTransaction(): List<Transaction> {
        return transactionDao.getTransaction()
    }

    @Transactional
    override fun remove(id: Long): Transaction {
        val transaction = transactionDao.findById(id)
        transaction?.isDeleted = true
        return transaction
    }

    override fun save(transDto: TransactionDto): Transaction {
        val transaction = MapperUtil.INSTANCE.mapTransactionDto(transDto)
        return transactionDao.save(transaction)
    }

    @Autowired
    lateinit var walletDao: WalletDao

    @Transactional
    override fun save(walletID: Long, transDto: Transaction): Transaction {
        val wallet = walletDao.findById(walletID)
        val transaction = transactionDao.save(transDto)
        wallet?.transactions?.add(transaction)
        if (transaction.transactionStatus == TransactionStatus.INCOME) {
            wallet.budget = wallet.budget!!.plus(transaction.amountOfMoney!!)
            } else {
            wallet.budget = wallet.budget!!.minus(transaction.amountOfMoney!!)
            }
        return transaction

    }
}





