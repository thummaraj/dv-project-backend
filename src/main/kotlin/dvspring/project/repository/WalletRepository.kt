package dvspring.project.repository

import dvspring.project.entity.Transaction
import dvspring.project.entity.TransactionCategoryStatus
import dvspring.project.entity.TransactionStatus
import dvspring.project.entity.Wallet
import org.springframework.data.repository.CrudRepository

interface WalletRepository : CrudRepository<Wallet,Long>{
    fun findByWalletNameContainingIgnoreCase(name: String): List<Wallet>
    fun findByTransactions_TransactionStatus(status: TransactionStatus): List<Wallet>
    fun findByTransactions_TransactionCategoryStatus(category : TransactionCategoryStatus): List<Wallet>
    fun findByIsDeletedIsFalse(): List<Wallet>
}