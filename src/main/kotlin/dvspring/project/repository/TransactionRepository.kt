package dvspring.project.repository

import dvspring.project.entity.Transaction
import dvspring.project.entity.TransactionStatus
import dvspring.project.entity.dto.TransactionDto
import org.springframework.data.repository.CrudRepository
import java.time.LocalDate

interface TransactionRepository: CrudRepository<Transaction,Long> {
    fun findByIsDeletedIsFalse(): List<Transaction>
     fun findByTransactionStatus(status: TransactionStatus): List<Transaction>
    fun findByDate(date: String): List<Transaction>
}