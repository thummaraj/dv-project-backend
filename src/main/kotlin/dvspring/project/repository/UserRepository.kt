package dvspring.project.repository

import dvspring.project.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<User,Long> {
    fun findByIsDeletedIsFalse(): List<User>
}